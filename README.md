# Creatividad y Arte Libre

## Resumen.

Este proyecto es el resultado de una migración en cuanto a diseño grafico, del software privativo al software libre. En el curso se presentaron una serie de alternativas a gran cantidad de programas privativos en este trabajo exploraremos dos de ellas (GIMP e Inskape).

## Presentación y ante proyecto.

En el apartado de [Documentos](/Documentos) se encuentran ubicados el anteproyecto y una presentación que sucinta el proceso de creación de las piezas graficas.

## Editables.

En el apartado de [Editables](/Editables) se encuentran los archivos con sus respectivas extensiones en caso de que se quiera ver la estructura u organización del diseño en el programa, o como ejemplo para alguien que quiera lograr algo similar. 

## Recursos.

En [Recursos](/Recursos) se encuentran las imágenes que se usaron para el montaje fotográfico, imagenes sacadas del banco de imagenes libre [Pexels](https://www.pexels.com/es-es/) y [Pixabay](https://pixabay.com/es/). 

## Resultados.

En el apartado de [entregables](/Entregables), se encuentran en formato de imagen los productos obtenidos en este proyecto.

![imagen.png](./imagen.png)

***

# Conclusión.

La falta de información es nuestro mayor problema, para alguien que ha realizado todos sus diseños en software privativo por falta de conocimiento de alternativas o por creer que al ser privativo es mejor, fue de gran sorpresa encontrar que en las alternativas libres una vez comprendes los atajos y conceptos básicos no existen limitaciones respecto a su competencia y se puede llegar a obtener resultados de la misma calidad. 

Este proyecto fue de gran ayuda para mi, ya que el software privativo si tenia ciertas limitaciones en cuanto a su adquisición, con estas alternativas estos problemas desaparecen. Me gustaría que mas personas probaran este tipo de alternativas libres y comiencen a migrar para hacer crecer el mundo de la Creatividad y el Arte Libre.

